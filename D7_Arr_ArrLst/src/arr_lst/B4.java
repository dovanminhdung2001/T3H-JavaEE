package arr_lst;

import java.util.ArrayList;

public class B4 {
    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<>();

        arrList.add(1);
        arrList.add(4);
        arrList.add(7);
        arrList.add(3);
        arrList.add(6);
        System.out.println(getHighScore(arrList));
    }

    static int getHighScore(ArrayList<Integer> scoreBoard) {
        int max = scoreBoard.get(0);
        for (Integer x: scoreBoard){
            if (max < x) {
                max = x;
            }
        }
        return max;
    }
}
