package arr_lst;

import java.util.ArrayList;

public class B13 {
    public static void main(String[] args) {
        ArrayList<String> value1 = new ArrayList<>();
        ArrayList<String> value2 = new ArrayList<>();

        value1.add("a");
        value1.add("a");
        value1.add("b");
        value1.add("e");
        value1.add("f");
        value2.add("a");
        value2.add("a");
        value2.add("b");
        value2.add("c");
        value2.add("d");
        allValuesInOnlyOneOf(value1, value2).forEach(System.out::println);
    }

    static ArrayList<String> allValuesInOnlyOneOf (
            ArrayList<String> value1,
            ArrayList<String> value2){
        ArrayList<String> result = new ArrayList<>();

        for (String x: value1){
            if (!value2.contains(x)){
                result.add(x);
            }
        }
        for (String x: value2){
            if (!value1.contains(x)){
                result.add(x);
            }
        }
        return result;
    }
}
