package arr_lst;

import java.util.ArrayList;

public class B10 {
    public static void main(String[] args) {
        ArrayList<String> value1 = new ArrayList<>();
        ArrayList<String> value2 = new ArrayList<>();

        value1.add("a");
        value1.add("a");
        value1.add("b");
        value1.add("e");
        value1.add("f");
        value2.add("a");
        value2.add("a");
        value2.add("b");
        value2.add("c");
        value2.add("d");
        commonValues(value1, value2).forEach(System.out::println);
    }

    static ArrayList<String> commonValues(
            ArrayList<String> values1,
            ArrayList<String> values2) {
        ArrayList<String> result = new ArrayList<>();

        for (String x: values1) {
            if (values2.contains(x) && !result.contains(x)) {
                result.add(x);
            }
        }
        return result;
    }
}
