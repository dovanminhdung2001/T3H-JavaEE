package arr_lst;

import java.util.ArrayList;

public class B5 {
    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<>();
        double sum = 0;

        arrList.add(1);
        arrList.add(4);
        arrList.add(7);
        arrList.add(3);
        arrList.add(6);
        for (Integer value : arrList){
            sum += value;
        }
        System.out.println(sum / arrList.size());
    }
}
