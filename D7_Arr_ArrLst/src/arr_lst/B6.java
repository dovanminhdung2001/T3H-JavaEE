package arr_lst;

import java.util.ArrayList;

public class B6 {
    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<>();

        arrList.add(1);
        arrList.add(4);
        arrList.add(7);
        arrList.add(3);
        arrList.add(6);
        arrList = reverseScores(arrList);
        arrList.forEach(System.out::println);
    }

    static ArrayList<Integer> reverseScores(ArrayList<Integer> scoreBoard){
        int temp;

        for (int i = 0; i < scoreBoard.size() / 2; i++) {
            temp = scoreBoard.get(i);
            scoreBoard.set(i, scoreBoard.get(scoreBoard.size() - 1 - i));
            scoreBoard.set(scoreBoard.size() - 1 - i, temp);
        }
        return scoreBoard;
    }
}
