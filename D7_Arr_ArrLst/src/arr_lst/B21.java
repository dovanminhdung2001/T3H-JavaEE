package arr_lst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class B21 {

    /* convert an ArrayList to an array */
    public static void main(String[] args) {
        List<Integer> arrLst = new ArrayList<>();
        Integer[] result ;

        arrLst.add (3);
        arrLst.add (4);
        arrLst.add (5);
        result = arrLst.toArray(new Integer[0]);
        System.out.println(Arrays.toString(result));
    }
}
