package arr_lst;

import java.util.ArrayList;

public class B9 {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();

        words.add("sahara");
        words.add("hanna");
        words.add("sahara");
        words.add("vietnamese");
        words.add("hanna");
        words.add("component");
        words.add("component");
        words = removeDuplicates(words);
        words.forEach(System.out::println);
    }

    static ArrayList<String> removeDuplicates(ArrayList<String> values) {
        ArrayList<String> result = new ArrayList<>();

        result.add(values.get(0));
        for (int i = 1; i < values.size(); i++) {
            if (!result.contains(values.get(i))){
                result.add(values.get(i));
            }
        }
        return result;
    }
}
