package arr_lst;

import java.util.ArrayList;
import java.util.List;

public class B20 {

    /* 20 convert an array to ArrayList */
    static ArrayList<Integer> toArrayList(int[] arr) {
        List<Integer> arrayList = new ArrayList<>();

        for (int j : arr) {
            arrayList.add(j);
        }
        return (ArrayList<Integer>) arrayList;
    }

    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4};

        System.out.println(toArrayList(a));
    }
}
