package arr_lst;

import java.util.ArrayList;

public class B3 {
    public static void main(String[] args) {
        ArrayList<Integer> arrList = new ArrayList<>();

        arrList.add(1);
        arrList.add(4);
        arrList.add(7);
        arrList.add(3);
        arrList.add(6);
        System.out.println(sumScores(arrList));
    }
    static int sumScores(ArrayList<Integer> scoreBoard) {
        int sum = 0;
        for (Integer x: scoreBoard){
            sum += x;
        }
        return  sum;
    }
}
