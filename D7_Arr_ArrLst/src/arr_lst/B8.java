package arr_lst;

import java.util.ArrayList;

public class B8 {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        String x = "hello";

        words.add("hanna");
        words.add("sahara");
        words.add("vietnamese");
        words.add("component");
        words.add("tag");
        words.add("environment");
       // System.out.println(words.indexOf("hi"));
        System.out.println(findValue(words, x));
    }

    public static int findValue(ArrayList<String> values, String x) {
        for (int i = 0; i < values.size(); i++) {
            if (x.equalsIgnoreCase(values.get(i))){
                return i;
            }
        }
        return -1;
    }
}
