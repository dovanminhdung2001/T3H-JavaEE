package arr_lst;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class B1_B2 {

    public static void main(String[] args) {

        /* 1 */
        String[] arr = {"Sunday", "Monday", "Tuesday", "Wednesday"
                , "Thursday", "Friday", "Saturday"};

        for (String value : arr) {
            System.out.println(value);
        }

        /* 2 */
        List<String> arrList = new ArrayList<>();

        Collections.addAll(arrList, arr);
        arrList.forEach(System.out::println);
    }
}
