package arr_lst;

import java.util.ArrayList;

public class B7 {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        int minLength = 6;

        words.add("hanna");
        words.add("sahara");
        words.add("vietnamese");
        words.add("component");
        words.add("tag");
        words.add("environment");
        words = removeShortWords(words, minLength);
        words.forEach(System.out::println);
    }

    static ArrayList<String> removeShortWords(ArrayList<String> words, int minLength) {
        ArrayList<String> longWords = new ArrayList<>();
        for (String x: words){
            if (x.length() >= minLength){
                longWords.add(x);
            }
        }

        return longWords;
    }
}
