package arr.b1b3;

public class B1_arr {

    /* bubble sort */
    static Student[] bubbleSort(Student[] arr) {
        boolean sorted = false;
        Student temp;

        while (!sorted) {
            sorted = true;

            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i].getName().compareToIgnoreCase(arr[i+1].getName()) > 0) {
                    temp = arr[i];
                    arr[i] = arr[i + 1] ;
                    arr[i + 1] = temp;
                    sorted = false;
                }
            }
        }
        return arr;
    }

    /* insertion sort */
    static Student[] insertionSort(Student[] a) {
        for (int i = 1; i < a.length; i++) {
            Student curr = a[i];
            int j =i-1;

            while (j >= 0 && curr.getName().compareToIgnoreCase(a[j].getName()) < 0) {
                a[j+1] = a[j];
                j--;
            }
            a[j+1] = curr;
        }
        return a;
    }

    /* selectionSort */
    static Student[] selectionSort(Student[] a) {
        for (int i = 0; i < a.length; i++) {
            Student min = a[i];
            int minId = i;

            for (int j = i + 1; j < a.length; j++) {
                if(a[j].getName().compareToIgnoreCase(min.getName()) < 0) {
                    min = a[j];
                    minId = j;
                }
            }
            Student temp = a[i];
            a[i] = min;
            a[minId] = temp;
        }
        return a;
    }

    static void showStudentsArr (Student[] arr){
        for (Student x: arr) {
            System.out.println(x.toString());
        }
    }

    public static void main(String[] args) {
        Student sv1 = new Student("CT0405","manh hao","cong nghe thong tin");
        Student sv2 = new Student("At160302","van phuc","an toan thong tin");
        Student sv3 = new Student("DT030201","thi nguyet","dien tu vien thong");
        Student sv4 = new Student("NN120301","kim lien","ngon ngu hoc");
        Student sv5 = new Student("CK250812","tuan thanh","co khi o to");
        Student sv6 = new Student("ML270712","anh tuyet","machine learning");
        Student[] arr = new Student[6];
        arr[0] = sv1;
        arr[1] = sv2;
        arr[2] = sv3;
        arr[3] = sv4;
        arr[4] = sv5;
        arr[5] = sv6;

        showStudentsArr(arr);
        bubbleSort(arr);
        for(int i = 0; i < 6; i++){
            System.out.println(arr[i].toString());
        }
    }
}
