package arr;

import java.util.Arrays;

public class B12_arr {

    /* 12.find the duplicate values of an array of integer values */
    static String findDuplicateInt(int[] arr) {
        if (arr.length < 2) {
            return "Not exist";
        } else {
            StringBuilder result = new StringBuilder();

            Arrays.sort(arr);
            for (int i = 0; i <= arr.length - 2; i++) {
                if ((arr[i] == arr[i + 1])
                        && (i == arr.length - 2 || (arr[i] != arr[i + 2]))) {
                    result.append(arr[i]).append(", ");
                }
            }
            return result.toString().equals("")
                    ? "Not exist"
                    : result.toString();
        }
    }

    public static void main(String[] args) {
        int[] a = {1, 5, 2, 2, 4, 2, 5, 4};  
        int[] b = {1};
        int[] c = {1, 1};
        int[] d = {1, 2, 3};

        System.out.println(findDuplicateInt(d));
    }
}
