package arr;

import java.util.Arrays;

public class B59_arr {

    /* find maximum product of two integers in a given array of integers */
    static  String findMaxProductOf2Int(int[] arr) {
        if (arr.length < 2){
            return "arr is not valid";
        }
        Arrays.sort(arr);
        return String.format("Pair is: %d, %d\nMax product: %d"
                , arr[arr.length - 1], arr[arr.length - 2], arr[arr.length - 1] * arr[arr.length - 2]);
    }


    public static void main(String[] args) {
        int[] arr = {1, 3, 7, 2, 8};

        System.out.println(findMaxProductOf2Int(arr));
    }
}
