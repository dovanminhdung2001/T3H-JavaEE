package arr;

import java.util.Arrays;

public class B7_arr {

    /*  7. remove a specific element from an array */
    static int[] remove(int[] arr, int id) {
        int[] newArr = new int[arr.length - 1];

        if (id >= 0) {
            System.arraycopy(arr, 0, newArr, 0, id);
        }
        if (arr.length - 1 - id >= 0) {
            System.arraycopy(arr, id + 1, newArr, id, arr.length - 1 - id);
        }
        return newArr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 4, 2, 4};
        int id = 1;

        System.out.println(Arrays.toString(remove(arr, id)));
    }
}
