package arr;

public class B2_arr {

    /* 2. sum values of an array */
    static int sumOf(int[] arr) {
        int sum = arr[0];

        for (int i = 1; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        System.out.println();
    }
}
