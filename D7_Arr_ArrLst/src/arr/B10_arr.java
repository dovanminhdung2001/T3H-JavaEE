package arr;

public class B10_arr {

    /* 10. find the maximum and minimum value of an array */
    static int findMin(int[] arr) {
        int min = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        return min;
    }

    static int findMax(int[] arr) {
        int max = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < max) {
                max = arr[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] a = {1, 4, 2, 5, 4};

        System.out.printf("min: %d \nmax: %d", findMin(a), findMax(a));
    }
}
