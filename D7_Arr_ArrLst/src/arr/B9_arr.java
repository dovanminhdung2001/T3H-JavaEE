package arr;

import java.util.Arrays;

public class B9_arr {

    /*  9. insert an element (specific position) into an array. */
    static int[] insert(int[] arr, int id, int value) {
        int[] newArr = new int[arr.length+1];

        if (id >= 0) {
            System.arraycopy(arr, 0, newArr, 0, id);
        }
//        for (int i = 0; i < id; i++) {
//            b[i] = a[i];
//        }

        if (arr.length - id >= 0) {
            System.arraycopy(arr, id, newArr, id + 1, arr.length - id);
        }
//        for (int i = id; i < a.length; i++) {
//            b[i + 1] = a[i];
//        }
        newArr[id] = value;
        return newArr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 4, 2, 4};
        int id  = 1;
        int value = 5;

        System.out.println(Arrays.toString(insert(arr, id, value)));
    }
}
