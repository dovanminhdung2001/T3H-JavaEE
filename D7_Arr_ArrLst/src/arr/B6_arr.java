package arr;

public class B6_arr {

    /* 6. find the index of an array element. */
    static int findId(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (value == arr[i]) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {0, 2, 4, 6, 8};
        int value = 4;

        System.out.println(findId(arr, value));
    }
}
