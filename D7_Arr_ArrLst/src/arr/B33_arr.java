package arr;


import java.util.Arrays;

public class B33_arr {

    /*
     * 33. remove the duplicate elements of a given array and return
     * the new length of the array.
     */
    static int rmvDuplicateElements(int[] arr) {
        int length = 1;

        Arrays.sort(arr);
        for (int i = 1; i < arr.length; i++) {
            boolean isExisted = false;

            for (int j = i - 1; j >= 0; j--) {
                if (arr[i] == arr[j]) {
                    isExisted = true;
                    break;
                }
            }
            if (!isExisted) {
                length++;
            }
        }
        return length;
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3};

        System.out.println(rmvDuplicateElements(arr));
    }
}
