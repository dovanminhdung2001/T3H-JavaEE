package arr;

public class B16_arr {

    /* 16. remove duplicate elements from an array. */
    static String removeDuplicate(int[] arr) {
        StringBuilder result = new StringBuilder();

        result.append(arr[0]).append(", ");
        for (int i = 1; i < arr.length; i++) {
            boolean isExisted = false;

            for (int j = i - 1; j >= 0; j--) {
                 if (arr[i] == arr[j]) {
                     isExisted = true;
                    break;
                 }
            }
            if (!isExisted) {
                result.append(arr[i]).append(", ");
            }
        }
        return result.toString();
    }
    public static void main(String[] args) {
        int[] arr = {1, 5, 2, 2, 4, 2, 5, 4};

        System.out.println(removeDuplicate(arr));
    }
}
