package arr;

public class B23_arr {

    /* 23. test the equality of two arrays */
     static String cmpArr(int[] arr1,int[] arr2){
        if (arr1.length != arr2.length){
            return "false";
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return "false";
            }
        }
        return "true";
    }


    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {2, 2, 3};
        int[] arr3 = {1, 2};
        int[] arr4 = {1, 2, 3};

        System.out.println(cmpArr(arr1, arr4));
    }
}
