package arr;

public class B27_arr {

    /* 27. find the number of even and odd integers in a given array of integers. */
    static String numOfEvenAndOddInt(int[] arr) {
        int countOddNumber = 0;
        int countEvenNumber = 0;

        for (int x : arr) {
           if (x % 2 == 1){
               countOddNumber++;
           } else {
               countEvenNumber++;
           }
        }
        return String.format("odd: %d int\neven: %d int", countOddNumber, countEvenNumber);
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 4, 6, 5};

        System.out.println(numOfEvenAndOddInt(arr));
    }
}
