package arr;

import java.util.Arrays;

public class B13_arr {

    /* 13. find the duplicate values of an array of string values */
    static String findDuplicateChar(char[] arr) {
        if (arr.length < 2){
            return "Not exist";
        } else {
            StringBuilder result = new StringBuilder();

            Arrays.sort(arr);
            for (int i = 0; i <= arr.length - 2; i++) {
                if ((arr[i] == arr[i + 1])
                        && (i == arr.length - 2 || (arr[i] != arr[i + 2]))){
                    result.append(arr[i]).append(", ");
                }
            }
            return result.toString().equals("")
                    ? "Not exist"
                    : result.toString();
        }
    }
    public static void main(String[] args) {
        char[] arr = {'a', 'b', 'c', 'a', 'b', 'a'};

        System.out.println(findDuplicateChar(arr));
    }
}
