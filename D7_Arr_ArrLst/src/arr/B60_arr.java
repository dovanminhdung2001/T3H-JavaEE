package arr;

import java.util.Arrays;

public class B60_arr {

    /* 60. shuffle a given array of integers */
    static int[] shuffleArr(int[] arr) {
        int j;

        for (int i = 0; i < arr.length; i++) {
            j = (int) (Math.random() * arr.length);
            arr[i] = arr[i] + arr[j];
            arr[j] = arr[i] - arr[j];
            arr[i] = arr[i] - arr[j];
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        System.out.println(Arrays.toString(shuffleArr(arr)));
    }
}
