package arr;

import java.util.Arrays;

public class B41_arr {

    /* 41. find smallest and second-smallest elements of a given array. */
    static String findMinAnd2ndMin(int[] arr) {
        if(arr.length == 0){
            return "empty arr";
        }
        if (arr.length == 1){
            return "min =" + arr[0] + ", none 2nd min";
        }
        StringBuilder result = new StringBuilder("min: ");

        Arrays.sort(arr);
        result.append(arr[0]).append("\n");
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[i - 1]){
                result.append("2nd min: ").append(arr[i]);
                return result.toString();
            }
        }
        result.append("none 2nd min");
        return result.toString();
    }

    public static void main(String[] args) {
        int[] arr = {5, 4, 6, 3, 7};

        System.out.println(findMinAnd2ndMin(arr));
    }
}
