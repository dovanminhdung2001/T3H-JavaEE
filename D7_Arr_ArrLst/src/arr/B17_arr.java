package arr;

import java.util.Arrays;

public class B17_arr {

    /* 17. find the second-largest element in an array	 */
    static String find2ndLargestElement(int[] arr){
        if (arr.length < 2){
            return "none";
        }
        Arrays.sort(arr);
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] < arr[arr.length - 1]) {
                return ""+arr[i];
            }
        }
        return "none";
    }

    public static void main(String[] args) {
        int[] arr = {1, 5, 2, 2, 4, 2, 5, 4};

        System.out.println(find2ndLargestElement(arr));
    }
}
