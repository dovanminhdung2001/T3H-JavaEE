package arr;

import java.util.Arrays;

public class B11_arr {

    /*   11.reverse an array of integer values */
    static int[] reverse(int[] arr) {
        int[] newArr = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            newArr[i] = arr[arr.length - 1 - i];
        }
        return newArr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 4, 2, 5, 4};

        System.out.println(Arrays.toString(reverse(arr)));
    }
}
