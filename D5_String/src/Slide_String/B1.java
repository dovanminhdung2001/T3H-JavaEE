package Slide_String;

public class B1 {
    static StringBuilder bai1(){
        char[] array = {'H','e','l','l','o'};
        StringBuilder builder = new StringBuilder();

        for (char value : array) {
            builder.append(value);
        }
        return builder;
    }

    public static void main(String[] args) {
        System.out.println(bai1());
    }
}
