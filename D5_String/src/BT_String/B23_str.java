package BT_String;

public class B23_str {

    /* 23.Write a java program to remove a newline in java */
    static String removeNewLine(String s) {
        String[] arr = s.split("\n");
        StringBuilder sBuilder = new StringBuilder();

        for (String value : arr) {
            sBuilder.append(value);
        }
        s = sBuilder.toString();
        return s;
    }

    public static void main(String[] args) {
        String s = "a\nb\nc\n\nd";

        System.out.println(removeNewLine(s));
    }
}
