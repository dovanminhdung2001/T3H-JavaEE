package BT_String;

import org.jetbrains.annotations.NotNull;

public class B57_str {

    /* 57 	 remove spaces from a given string */
    static @NotNull String rmvSpace(String s) {
        s = s.trim();
        String[] arr = s.split(" ");
        StringBuilder sBuilder = new StringBuilder();

        for (String value : arr) {
            sBuilder.append(value);
        }
        return sBuilder.toString();
    }

    public static void main(String[] args) {
        String s = " ";

        System.out.println(rmvSpace(s));
    }
}