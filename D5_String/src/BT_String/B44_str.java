package BT_String;

public class B44_str {

    /* 44. print the index of the character in a string */
    static void printIndexOfAllChar(String s) {
        for(int i = 0; i < s.length(); i++) {
            System.out.println("Current character " +s.charAt(i)+ " position at " +i);
        }
    }

    public static void main(String[] args) {
        String s ="halo";

        printIndexOfAllChar(s);
    }
}
