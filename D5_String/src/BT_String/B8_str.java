package BT_String;

import org.jetbrains.annotations.NotNull;

public class B8_str {

    /*
     * 8. takes a list of words and return the longest word and the length
     * of the longest one
     */
    static void findLongestWord (@NotNull String s) {
        String[] arr = s.split(" ");
        int maxSize = arr[0].length();
        int id = 0;

        for (int i = 1; i < arr.length ; i++) {
            if (arr[i].length() > maxSize) {
                maxSize = arr[i].length();
                id = i;
            }
        }
        System.out.printf("Longest word: %s \n" +
                "Longest word: %d%n", arr[id], arr[id].length());
    }

    public static void main(String[] args) {
        String s = "as dsf";

         findLongestWord(s);
    }
}
