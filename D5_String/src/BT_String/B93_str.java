package BT_String;

public class B93_str {
    /*
     * 	92. return a substring after removing the all instances of remove
     * string as given from the given main string.
     */
    static  String rmvSubStr(String str, String sub){
        str = str.replaceAll(sub, "");
        return str;
    }

    public static void main(String[] args) {
        String s = "google.com.gov";
        String sub = "go";

        System.out.printf(rmvSubStr(s,sub));
        System.out.printf("\n%010d",6);
    }
}
