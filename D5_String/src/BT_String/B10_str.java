package BT_String;

import org.jetbrains.annotations.NotNull;

public class B10_str {

    /*
     * 10. change a given string to a new string where the first
     * and last chars have been exchanged.
     */
    static @NotNull String exchangeFistAndLastChar(@NotNull String s) {
        char first = s.charAt(0);
        char last = s.charAt(s.length() - 1);
        s = last + s.substring(1, s.length() - 1) + first;

        return s;
    }

    public static void main(String[] args) {
        String s = "123 456";

        System.out.printf(exchangeFistAndLastChar(s));
    }
}
