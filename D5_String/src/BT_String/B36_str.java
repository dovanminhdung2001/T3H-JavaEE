package BT_String;

public class B36_str {

    /* 36. 	format a number with a percentage. */
    static String toPercentage(double f) {
        return  f * 100 + "%";
    }

    public static void main(String[] args) {
        double num = 8.0;

        System.out.println(toPercentage(num));
    }
}
