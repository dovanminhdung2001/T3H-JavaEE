package BT_String;

public class B28_str {

    /* 28. add a prefix text to all the lines in a string.	*/
    static String addPrefixText(String s, String prefix) {
        String[] arr = s.split("\n");
        StringBuilder sBuilder = new StringBuilder();

        for (String value : arr) {
            sBuilder.append(prefix).append(value).append("\n");
        }
        return sBuilder.toString();
    }

    public static void main(String[] args) {
        String s = "cover";
        String prefix = "re";

        System.out.println(addPrefixText(s, prefix));
    }
}
