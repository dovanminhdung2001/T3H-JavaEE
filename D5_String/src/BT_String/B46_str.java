package BT_String;

public class B46_str {

    /* 46. convert a given string into a list of words. */
    static String toListOfWords(String s) {
        String[] arr = s.split(" ");
        StringBuilder stringBuilder = new StringBuilder("[");

        for(int i = 0; i <arr.length-1; i++) {
            stringBuilder.append("'").append(arr[i]).append("', ");
        }
        stringBuilder.append("'").append(arr[arr.length-1]).append("'");
        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        String s ="im here, halo";

        System.out.println(toListOfWords(s));
    }
}
