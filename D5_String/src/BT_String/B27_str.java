package BT_String;

public class B27_str {

    /* 27. remove existing indentation from all the lines in a given text */
    static String removeIndentation(String s) {
        String[] arr = s.split("\n\t");
        StringBuilder sBuilder = new StringBuilder();

        for (String value : arr) {
            sBuilder.append(value).append("\n");
        }
        s = sBuilder.toString();
        return s;
    }

    public static void main(String[] args) {
        String s ="a\n\tb\nb\tc\n\td";

        System.out.println(removeIndentation(s));
    }
}
