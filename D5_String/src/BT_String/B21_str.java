package BT_String;

public class B21_str {

    /*
     * 21. convert a given string to all UpperCase if it contains at
     * least 2 UpperCase characters in the first 4 characters.
     */
    static String convertSuitableStr (String s) {
        int  count = 0;

        for(int i = 0; i < 3; i++) {
            if (s.charAt(i) > 64&&s.charAt(i) < 91) {
                count++;
            }
        }
        if (count >= 2) {
            return s.toUpperCase();
        }
        return s;
    }

    public static void main(String[] args) {
        String s ="asd sdd";

        System.out.println(convertSuitableStr(s));
    }}
