package BT_String;

public class B11_str {

    /*
     * 11. remove the characters which have odd index values of
     * a given string
     */
    static String removeOddIndexChar(String s) {
        StringBuilder s2 = new StringBuilder();

        for(int i = 1; i <= s.length() /2; i++) {
            s2.append(s.charAt(2 * i - 1));
        }
        return s2.toString();
    }

    public static void main(String[] args) {
        String s =" ";

        System.out.printf(removeOddIndexChar(s));
    }
}
