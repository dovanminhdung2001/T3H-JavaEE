package BT_String;

public class B2_str {

    /*2. 	count the number of characters (character frequency) in a string */
    static int idInStr (String s, int id) {
        for(int i = 0; i <id; i++) {
            if (s.charAt(i) == s.charAt(id)) {
                return i;
            }
        }
        return -1;
    }

    static void countChar (String s) {
        int[] arr = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            int id = idInStr(s, i);
            if (id == -1) {
                arr[i] = 1;
            } else {
                arr[id]++;
                arr[i] = 0;
            }
        }
        for (int i = 0; i < s.length(); i++) {
            if (arr[i] != 0) {
                System.out.print("'" + s.charAt(i) + "': " + arr[i] + ", ");
            }
        }
    }

    public static void main(String[] args) {
        String s = "hal-uu";

        countChar(s);
    }
}
