package BT_String;

import org.jetbrains.annotations.NotNull;

public class B5_str {

    /*
     * 5. get a single string from two given strings, separated by a space and
     *	swap the first two characters of each string.
     */
    static @NotNull String strConcat(@NotNull String str1, @NotNull String str2) {
        String head1 = str1.substring(0, 2);
        String head2 = str2.substring(0, 2);
        str1 = head2 + str1.substring(2);
        str2 = head1 + str2.substring(2);

        return  str2 + " " + str1;
    }

    public static void main(String[] args) {
        String s1 = "acb def";
        String s2 = "ijk mno";

        System.out.printf(strConcat(s1,s2));
    }
}
