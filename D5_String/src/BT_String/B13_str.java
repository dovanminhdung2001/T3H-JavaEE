package BT_String;

import java.util.Scanner;

public class B13_str {

    /*
     * 13. that takes input from the user and displays that input
     * back in upper and lower cases
     */
    static void displayUpperAndLoweCase() {
        Scanner sc  = new Scanner(System.in);
        String s;

        System.out.println("Nhap xau: ");
        s = sc.nextLine();
        System.out.printf("String: %s \nUpper case: %s \n Lower case: %s%n"
                , s, s.toUpperCase(), s.toLowerCase());
        sc.close();
    }

    public static void main(String[] args) {
        displayUpperAndLoweCase();
    }
}
