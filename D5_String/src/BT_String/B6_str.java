package BT_String;

import org.jetbrains.annotations.NotNull;

public class B6_str {

    /*
     *	6. add 'ing' at the end of a given string (length should be at least 3)
     *	If the given string already ends with 'ing' then add 'ly' instead. If
     *	the string length of the given string is less than 3, leave it unchanged.
     */
    static @NotNull String addIng(@NotNull String s) {
        if(s.length() < 3){
            return s;
        }
        String tail = s.substring(s.length()-3);

        return (tail.equalsIgnoreCase("ing"))
                ? s + "ly"
                : s + "ing";
    }

    public static void main(String[] args) {
        String s ="as dsf";

        System.out.printf(addIng(s));
    }
}
