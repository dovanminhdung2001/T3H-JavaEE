package BT_String;

public class B37_str {

    /*
     * 37. Write a java program to display a number in left, right and
     * center aligned of width 10
     */
    static void displayNum(int a) {
        System.out.printf("%10d\n",a);
        System.out.printf("%-10d\n",a);
    }

    public static void main(String[] args) {
        int num = 9;

        displayNum(num);
    }


}
