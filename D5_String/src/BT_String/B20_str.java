package BT_String;

import org.jetbrains.annotations.NotNull;

public class B20_str {

    /* 20. reverses a string if it's length is a multiple of 4	*/
    static @NotNull String reverseStr(@NotNull String s) {
        StringBuilder b = new StringBuilder();

        if (s.length() % 4 != 0) {
            return s;
        }
        for(int i = s.length()-1; i >= 0; i--) {
            b.append(s.charAt(i));
        }
        return b.toString();
}

    public static void main(String[] args) {
        String s = "sdf sdk";

        System.out.println(reverseStr(s));
    }
}
