package BT_String;

public class B40_str {

    /* 40. reverse words in a string */
    static String reverseWord (String s) {
        String[] arr = s.split(" ");
        StringBuilder sBuilder = new StringBuilder();
        for (int i = arr.length-1; i >= 0; i--) {
            sBuilder.append(arr[i]).append(" ");
        }
        return  sBuilder.toString();
    }

    public static void main(String[] args) {
        String s ="hello world";

        System.out.println(reverseWord(s));
    }
}
