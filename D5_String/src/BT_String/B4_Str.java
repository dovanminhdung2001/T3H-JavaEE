package BT_String;

import org.jetbrains.annotations.NotNull;

public class B4_Str {

    /*
     * 4. get a string from a given string where all occurrences of its first
     *	char have been changed to '$', except the first char itself
     */
    static @NotNull String  changeTo$(@NotNull String s) {
        char c = s.charAt(0);

        s = s.replace(s.charAt(0), '$');
        s = c + s.substring(1);
        return s;
    }

    public static void main(String[] args) {
        String s = "nano";

        System.out.printf(changeTo$(s));
    }
}
