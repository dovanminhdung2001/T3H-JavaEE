package BT_String;

import org.jetbrains.annotations.NotNull;

public class B14_str {

    /*
     * 14. accepts a comma separated sequence of words as input
     * and prints the unique words in sorted form (alphanumerically)
     */
    static String[] toSortedListOfWords(String[] arr) {
        String temp;

        for(int i = 0; i < arr.length; i++) {
            for(int j = i; j >= 0; j--) {
                if (arr[i].compareTo(arr[j]) > 0) {
                    temp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = temp;
                }
            }
        }
        return arr;
    }

    static String uniqueWords(@NotNull String s) {
        String[] arr = s.split(", ");
        arr = toSortedListOfWords(arr);
        StringBuilder sBuilder = new StringBuilder(arr[arr.length - 1] + ", ");

        for(int i = arr.length-2; i >= 0; i--) {
            if (!arr[i].equalsIgnoreCase(arr[i+1])) {
                sBuilder.append(arr[i]).append(", ");
            }
        }
        return sBuilder.toString();
    }

    public static void main(String[] args) {
        String s ="when you are studying you sleep ";

        System.out.printf(uniqueWords(s));
    }
}
