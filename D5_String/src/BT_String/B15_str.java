package BT_String;

public class B15_str {

    /*15. create the HTML string with tags around the word(s)*/
    static String addTag(String tag, String content) {
        return String.format("<%s>%s</%s>", tag, content, tag);
    }

    public static void main(String[] args) {
        String s = "halo";
        String tag = "a";

        System.out.println(addTag(tag,s));
    }
}
