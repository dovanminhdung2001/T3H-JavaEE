package BT_String;

public class B24_str {

    /* 24. check whether a string starts with specified characters*/
    static boolean startedWithSpecifiedChar(String s) {
        return s.matches("^\\W.*");
    }

    public static void main(String[] args) {
        String s = "asd ads";

        System.out.printf("string starts with specified characters: %b", startedWithSpecifiedChar(s));
    }
}
