package exception;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Vd1 {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws CustomException {
        List<Student> list = new ArrayList<>();

        list.add(new Student("s1", "sinhvien1", LocalDate.parse("2001-01-02")));
        list.add(new Student("s2", "sinhvien2", LocalDate.parse("2001-02-02")));
        list.add(new Student("s3", "sinhvien3", LocalDate.parse("2001-03-02")));
        list.add(new Student("s4", "sinhvien4", LocalDate.parse("2001-04-02")));
        list.add(new Student("s5", "sinhvien5", LocalDate.parse("2001-05-02")));
        addStudent(list);
        addStudent(list);
    }

    private static void addStudent(List<Student> list) throws CustomException {
        input.nextLine();
        String id = input.nextLine();
        String name = input.nextLine();
        int year = input.nextInt();
        int month = input.nextInt();
        int day = input.nextInt();
        Student student = new Student(
                id,
                name,
                LocalDate.of(
                        year,
                        month,
                        day
                )
        );
        if (list.size() > 6){
            throw new RuntimeException("dssv da day");
        } else if (student.getBirthDay().isBefore(LocalDate.parse("1970-01-01"))) {
            System.out.println("nam sinh khong hop le");
        } else if (student.getName().isBlank() || (student.getName() == null)){
            throw new CustomException("Name k dc de trong");
        } else {
            list.add(student);
            System.out.println("---Done---");
        }
    }
}
