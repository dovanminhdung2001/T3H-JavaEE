package slide;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ArrayList {
    public static void main(String[] args) {
        List<Integer> A = new java.util.ArrayList<>();
        List<Integer> B = new java.util.ArrayList<>();
        List<Integer> C = new java.util.ArrayList<>();
        List<Integer> D = new java.util.ArrayList<>();
        int value;
        Object[] arr;

        /* 1 */
        A.add(10);
        A.add(20);
        B.add(5);
        B.add(15);
        B.addAll(A);

        /* 2 */
        System.out.println("B is empty :" + B.isEmpty());

        /* 3 */
        for (int i = 0; i < 400; i++) {
            C.add(i);
        }
        value = 10;
        System.out.println(Arrays.binarySearch(C.toArray(), value) + "\n\n");

        /* 4 */
        D.add(1);
        D.add(2);
        D.add(3);
        D.add(4);
        D.replaceAll(ignored -> 101);


        /* 5 */
        D.add(10);
        D.add(20);
        D.sort(Comparator.naturalOrder()); //D.sort(Comparator.reverseOrder());
        int max = D.get(D.size() - 1);
        int min = D.get(0);

        /* 6 */
        for (int i = 0; i < B.size() / 2; i++) {
            int temp = B.get(i);
            B.set(i, B.get(B.size() - 1 - i));
            B.set(B.size() - 1 - i, temp);
        }
        for (int i = 0; i < C.size() / 2; i++) {
            int temp = C.get(i);
            C.set(i, C.get(C.size() - 1 - i));
            C.set(C.size() - 1 - i, temp);
        }

        /* 7 */
        for (int index = 0; index < D.size(); index++) {
            int index2 = (int) (Math.random() * D.size() -1);
            int temp = D.get(index);

            D.set(index, D.get(index2));
            D.set(index2, temp);
        }
    }
}