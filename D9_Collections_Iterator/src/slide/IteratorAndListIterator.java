package slide;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class IteratorAndListIterator {
    public static void main(String[] args) {
        List<Double> arrLst = new ArrayList<>();

        for (int i = 0; i < 11; i++) {
            arrLst.add(Math.random() * 220 - 100);
        }
        Iterator<Double> iterator = arrLst.iterator();
        ListIterator<Double> listIterator = arrLst.listIterator();

        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println("\n\nDone\n\n");
        while (listIterator.hasNext()){
            double value = listIterator.next();

            if ((value * 100 % 100 / 10) == 4){
                listIterator.remove();
            }
            if (value < 101){
                listIterator.add(Math.random() * 220 - 100);
            }
        }
        while (listIterator.hasPrevious()){
            System.out.println(listIterator.previous());
        }
    }
}
